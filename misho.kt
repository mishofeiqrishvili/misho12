import kotlin.math.sqrt

fun main(){

    var r = Point(9.0, 6.0)
    var c = Point(8.0, 2.0)
    println("coordinate1 : ${r.toString()}")
    println("coordinate2 : ${c.toString()}")
    println()
    println("coordinate1 ${r.equals(r)}")
    println("coordinate2 ${c.equals(c)}")
    println()
    println("$r INTO ${r.reverse()}")
    println("$c INTO ${c.reverse()}")
    println()
    println(" ${Mandzili(r,c)}")
}

class Point(private var x:Double,private var y:Double){
    fun first(): Double{
        return x
    }
    fun second(): Double{
        return y
    }
    override fun toString(): String {
        return "x -> $x  y -> $y"
    }

    override fun equals(other: Any?): Boolean {
        return (x==y)
    }
    fun reverse():String{
        x=-x
        y=-y
        return "x:$x && y:$y"
    }
}
fun Mandzili(r: Point, c: Point) :String{
    var p: Double = r.meore() - c.meore()
    var l: Double = r.pirveli() - c.pirveli()
    if (p<0) p=-p
    if (l<0) l=-l
    return "fesvi ${l*l+l*l}-dan == ${sqrt(p*p+l*l)}"
}

